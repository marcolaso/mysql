hostgroup :

        mysql> select * from mysql_servers ;
        +--------------+------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
        | hostgroup_id | hostname   | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
        +--------------+------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
        | 2            | 10.20.21.1 | 3306 | 0         | ONLINE | 10     | 0           | 300             | 0                   | 0       | 0              |         |
        | 2            | 10.20.21.2 | 3306 | 0         | ONLINE | 10     | 0           | 1000            | 0                   | 0       | 0              |         |
        | 2            | 10.20.21.3 | 3306 | 0         | ONLINE | 10     | 0           | 1000            | 0                   | 0       | 0              |         |
        +--------------+------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
        3 rows in set (0.01 sec)

        mysql>


regles :

        mysql> SELECT rule_id, active,match_digest, match_pattern, destination_hostgroup,  cache_ttl,multiplex, apply FROM mysql_query_rules;
        +---------+--------+----------------------+---------------+-----------------------+-----------+-----------+-------+
        | rule_id | active | match_digest         | match_pattern | destination_hostgroup | cache_ttl | multiplex | apply |
        +---------+--------+----------------------+---------------+-----------------------+-----------+-----------+-------+
        | 1       | 1      | ^SELECT.*            | NULL          | 3                     | NULL      | 1         | 0     |
        | 2       | 1      | ^SELECT.* FOR UPDATE | NULL          | 2                     | NULL      | NULL      | 1     |
        | 3       | 1      | ^SHOW.*              | NULL          | 3                     | NULL      | NULL      | 0     |
        | 4       | 0      | ^SELECT.*            | NULL          | 3                     | NULL      | NULL      | 0     |
        +---------+--------+----------------------+---------------+-----------------------+-----------+-----------+-------+
        4 rows in set (0.01 sec)

        mysql>

List des tables de stats :

        mysql> show tables from stats;

statistique sur les regles :

        mysql> select * from stats.stats_mysql_query_rules;
        +---------+-------+
        | rule_id | hits  |
        +---------+-------+
        | 1       | 42883 |
        | 2       | 0     |
        | 3       | 984   |
        | 4       | 42883 |
        +---------+-------+
        4 rows in set (0.02 sec)

        mysql>  


Statistique connexion / hostgrpoup :

        mysql>  SELECT * FROM stats.stats_mysql_connection_pool;
        +-----------+------------+----------+--------+----------+----------+--------+---------+-------------+------------+-------------------+-----------------+-----------------+------------+
        | hostgroup | srv_host   | srv_port | status | ConnUsed | ConnFree | ConnOK | ConnERR | MaxConnUsed | Queries    | Queries_GTID_sync | Bytes_data_sent | Bytes_data_recv | Latency_us |
        +-----------+------------+----------+--------+----------+----------+--------+---------+-------------+------------+-------------------+-----------------+-----------------+------------+
        | 2         | 10.20.21.3 | 3306     | ONLINE | 22       | 9        | 4880   | 0       | 219         | 1130494705 | 0                 | 184689469624    | 68058328899     | 219        |
        | 4         | 10.20.21.1 | 3306     | ONLINE | 0        | 0        | 0      | 0       | 0           | 0          | 0                 | 0               | 0               | 132        |
        | 4         | 10.20.21.2 | 3306     | ONLINE | 0        | 0        | 0      | 0       | 0           | 0          | 0                 | 0               | 0               | 341        |
        | 3         | 10.20.21.1 | 3306     | ONLINE | 333      | 11       | 7649   | 59      | 464         | 1371631870 | 0                 | 543754081933    | 31382884119681  | 132        |
        | 3         | 10.20.21.2 | 3306     | ONLINE | 148      | 10       | 9841   | 66      | 299         | 1117471906 | 0                 | 457085163370    | 22314085382125  | 341        |
        +-----------+------------+----------+--------+----------+----------+--------+---------+-------------+------------+-------------------+-----------------+-----------------+------------+
        5 rows in set (0.01 sec)

        mysql>

Statistique commands (select,insert,delete, etc...)

                mysql> SELECT * FROM stats_mysql_commands_counters where Command in ('SELECT','UPDATE','INSERT','DELETE','COMMIT','ROLLBACK');
                +----------+----------------+------------+-----------+------------+-----------+-----------+----------+----------+-----------+-----------+---------+---------+---------+----------+
                | Command  | Total_Time_us  | Total_cnt  | cnt_100us | cnt_500us  | cnt_1ms   | cnt_5ms   | cnt_10ms | cnt_50ms | cnt_100ms | cnt_500ms | cnt_1s  | cnt_5s  | cnt_10s | cnt_INFs |
                +----------+----------------+------------+-----------+------------+-----------+-----------+----------+----------+-----------+-----------+---------+---------+---------+----------+
                | COMMIT   | 20489275686    | 970255822  | 964477433 | 47283      | 6245      | 5312703   | 254798   | 145887   | 5927      | 4378      | 628     | 506     | 21      | 13       |
                | DELETE   | 5819237708     | 3135792    | 74        | 463750     | 1784531   | 875452    | 5826     | 3001     | 644       | 1979      | 170     | 210     | 114     | 41       |
                | INSERT   | 39403071440    | 136258557  | 25991     | 128229066  | 6208310   | 1675644   | 69472    | 41859    | 5847      | 2251      | 86      | 29      | 2       | 0        |
                | ROLLBACK | 640612         | 409349     | 406519    | 2764       | 49        | 16        | 1        | 0        | 0         | 0         | 0       | 0       | 0       | 0        |
                | SELECT   | 10903220571999 | 2785466120 | 949390    | 1672817404 | 878762199 | 201592641 | 9320764  | 14013945 | 1828742   | 323450    | 1859336 | 3885201 | 62612   | 50436    |
                | UPDATE   | 986886933983   | 630118563  | 151235    | 590564938  | 22905753  | 6360270   | 244282   | 103213   | 8235187   | 1534030   | 11757   | 4755    | 876     | 2267     |
                +----------+----------------+------------+-----------+------------+-----------+-----------+----------+----------+-----------+-----------+---------+---------+---------+----------+
                6 rows in set (0.02 sec)

                mysql>

Statistiques sur les erreurs :

        mysql> select * from stats.stats_mysql_errors;
        +-----------+------------+------+-------------+----------------+------------+-------+------------+------------+------------+--------------------------------------------------------------------------------------------+
        | hostgroup | hostname   | port | username    | client_address | schemaname | errno | count_star | first_seen | last_seen  | last_error                                                                                 |
        +-----------+------------+------+-------------+----------------+------------+-------+------------+------------+------------+--------------------------------------------------------------------------------------------+
        | 2         | 10.20.21.3 | 3306 | user_toto | 192.18.1.3    | db_1     | 1406  | 4          | 1600755782 | 1600755782 | Data too long for column 'token' at row 1                                                  |
        | 2         | 10.20.21.3 | 3306 | user_titi      | 192.18.1.4   | db_1       | 1142  | 2          | 1600755782 | 1600755782 | DROP command denied to user 'user_titi'@'cluster_node_2' for table 'trigger_metadata' |
        +-----------+------------+------+-------------+----------------+------------+-------+------------+------------+------------+--------------------------------------------------------------------------------------------+
        2 rows in set (0.02 sec)

        mysql>


Variables importantes :

        mysql>  show VARIABLES like 'mysql-free_connections_pct' ;
        +----------------------------+-------+
        | Variable_name              | Value |
        +----------------------------+-------+
        | mysql-free_connections_pct | 1     |
        +----------------------------+-------+
        1 row in set (0.00 sec)

        mysql>


        mysql>  show VARIABLES like '%multi%' ;
        +--------------------------------------+-------+
        | Variable_name                        | Value |
        +--------------------------------------+-------+
        | mysql-client_multi_statements        | true  |
        | mysql-connection_delay_multiplex_ms  | 0     |
        | mysql-multiplexing                   | true  |
        | mysql-auto_increment_delay_multiplex | 0     |
        +--------------------------------------+-------+
        4 rows in set (0.01 sec)

        mysql>


        mysql>  show VARIABLES like '%mysql-kill_backend_connection_when_disconnect%' ;
        +-----------------------------------------------+-------+
        | Variable_name                                 | Value |
        +-----------------------------------------------+-------+
        | mysql-kill_backend_connection_when_disconnect | true  |
        +-----------------------------------------------+-------+
        1 row in set (0.00 sec)

        mysql>


        mysql> show variables like 'mysql-ping_interval_server_msec';
        +---------------------------------+-------+
        | Variable_name                   | Value |
        +---------------------------------+-------+
        | mysql-ping_interval_server_msec | 60000 |
        +---------------------------------+-------+
        1 row in set (0.00 sec)

        mysql>

        mysql>  show variables like 'mysql-auto_increment_delay_multiplex' ;
        +--------------------------------------+-------+
        | Variable_name                        | Value |
        +--------------------------------------+-------+
        | mysql-auto_increment_delay_multiplex | 0     |
        +--------------------------------------+-------+
        1 row in set (0.00 sec)

        mysql> 

LOG : 

        mysql> SELECT hostname,port,ping_error FROM mysql_server_ping_log ;
        +--------------+------+--------------------------------------------------------------------------------+
        | hostname     | port | ping_error                                                                     |
        +--------------+------+--------------------------------------------------------------------------------+
        | 192.18.0.16 | 3306 | Host 'vmhapqa04.intra-tpg.ch' is not allowed to connect to this MariaDB server |
        | 192.18.0.15 | 3306 | Host 'vmhapqa04.intra-tpg.ch' is not allowed to connect to this MariaDB server |
        | 192.18.0.17 | 3306 | Host 'vmhapqa04.intra-tpg.ch' is not allowed to connect to this MariaDB server |

STATUS si SHUNNED = PB !!!

        mysql> select * from stats_mysql_connection_pool ;
        +-----------+--------------+----------+---------+----------+----------+--------+---------+-------------+---------+-------------------+-----------------+-----------------+------------+
        | hostgroup | srv_host     | srv_port | status  | ConnUsed | ConnFree | ConnOK | ConnERR | MaxConnUsed | Queries | Queries_GTID_sync | Bytes_data_sent | Bytes_data_recv | Latency_us |
        +-----------+--------------+----------+---------+----------+----------+--------+---------+-------------+---------+-------------------+-----------------+-----------------+------------+
        | 0         | 10.21.20.155 | 3306     | SHUNNED | 0        | 0        | 0      | 0       | 0           | 0       | 0                 | 0               | 0               | 0          |
        | 1         | 10.21.20.156 | 3306     | SHUNNED | 0        | 0        | 0      | 0       | 0           | 0       | 0                 | 0               | 0               | 0          |
        | 1         | 10.21.20.157 | 3306     | SHUNNED | 0        | 0        | 0      | 0       | 0           | 0       | 0                 | 0               | 0               | 0          |
        +-----------+--------------+----------+---------+----------+----------+--------+---------+-------------+---------+-------------------+-----------------+-----------------+------------+
        3 rows in set (0.01 sec)

        mysql> 


Save config

     # Save the current in-memory
     SAVE MYSQL VARIABLES TO DISK;
     SAVE ADMIN VARIABLES TO DISK;
     SAVE MYSQL QUERY RULES TO DISK;
     SAVE MYSQL USERS TO DISK;
     SAVE MYSQL SERVERS TO DISK;

     # Active current in-memory
     LOAD MYSQL USERS TO RUNTIME; 
     LOAD MYSQL SERVERS TO RUNTIME;
     LOAD MYSQL VARIABLES TO RUNTIME;
     LOAD MYSQL QUERY RULES TO RUNTIME; 
     
     

