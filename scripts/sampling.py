#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#############################################################################
# mysql query sampling
# if want 1 execute touch is_stop
# source mysql DB, destination : postgresql DB !!!
#
# exemple : sampling.py  -H DB_hostname  
#
# todo :
#    -
#
#############################################################################

# Import lib
import mysql.connector
from time import gmtime, strftime
import psycopg2
import argparse
import time
import os.path

#gestion des parametres
parser = argparse.ArgumentParser()
parser.add_argument(
    "-H",
    "--host",
    type=str,
    metavar="",
    required=True,
    help="hostname mysql server",
)
args = parser.parse_args()

#Initialisation des variables
is_stop="sampling_is_stop.lock"
interval=1
mysql_config_cnx = {
          'user': 'myqsl_username',
          'password': 'mysql_password',
          'host': args.host,
          'database': 'mysql',
}
pg_conf_cnx = "dbname='db_monitoring' user='u_monitoring' host='localhost' password='p_monitoring'"

DATE=strftime("%Y%m%d %H:%M:%S", gmtime())

# connexion aux DB
mysql_cnx = mysql.connector.connect(**mysql_config_cnx)
mysql_cursor = mysql_cnx.cursor()
pg_cnx = psycopg2.connect(pg_conf_cnx)
pg_cursor = pg_cnx.cursor()

# Recup data process mysql
mysql_query = ("SELECT *  FROM INFORMATION_SCHEMA.PROCESSLIST where COMMAND <> 'Sleep' and USER <> 'myqsl_username' order by id;")
mysql_cursor.execute(mysql_query)

while True:
  for (ID, USER, HOST, DB, COMMAND, TIME, STATE, INFO, TIME_MS, STAGE, MAX_STAGE, PROGRESS, MEMORY_USED, EXAMINED_ROWS, QUERY_ID, INFO_BINARY, TID) in mysql_cursor:
    # insert data date base histo
    INFO=str(INFO).replace("'",'"')
    INFO_BINARY=str(INFO_BINARY).replace("'",'"')
    pg_query="insert into sample_query \
          (DATE, SOURCE, ID, USERNAME, HOSTNAME, DBNAME, COMMAND, TIME, STATE, INFO, TIME_MS, STAGE, MAX_STAGE, PROGRESS, MEMORY_USED, EXAMINED_ROWS, QUERY_ID, INFO_BINARY, TID) \
          values ('"+DATE+"','"+args.host+"','"+str(ID)+"','"+str(USER)+"','"+str(HOST)+"','"+str(DB)+"','"+str(COMMAND)+"','"+str(TIME)+"','"+str(STATE)+"','"+str(INFO)+"','"+str(TIME_MS)+"','"+str(STAGE)+"','"+str(MAX_STAGE)+"','"+str(PROGRESS)+"','"+str(MEMORY_USED)+"','"+str(EXAMINED_ROWS)+"','"+str(QUERY_ID)+"','"+str(INFO_BINARY)+"','"+str(TID)+"') \
          ON CONFLICT DO NOTHING;"
    #print (pg_query+"\n")
    pg_cursor.execute(pg_query)
  pg_cnx.commit()
  if os.path.isfile(is_stop):
    break
  time.sleep(interval)

# close connexion DB
mysql_cursor.close()
mysql_cnx.close()
pg_cursor.close()
pg_cnx.close()
